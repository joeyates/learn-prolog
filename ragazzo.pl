ideale(R) :- alto(R), moro(R), laureato(R), sportivo(R).

% 1 ha 4 caratteristiche
ideale(benedetto) :-                       \+ ideale(carlo), \+ ideale(donato), \+ ideale(giovanni), \+ ideale(luca).
ideale(carlo) :-     \+ ideale(benedetto),                   \+ ideale(donato), \+ ideale(giovanni), \+ ideale(luca).
ideale(donato) :-    \+ ideale(benedetto), \+ ideale(carlo),                    \+ ideale(giovanni), \+ ideale(luca).
ideale(giovanni) :-  \+ ideale(benedetto), \+ ideale(carlo), \+ ideale(donato),                      \+ ideale(luca).
ideale(luca) :-      \+ ideale(benedetto), \+ ideale(carlo), \+ ideale(donato), \+ ideale(giovanni)                 .
% scambiano 'moro', o Carlo o Luca sarebbe ideale
ideale(carlo) :- moro(luca).
ideale(luca) :- moro(carlo).

ragazzo(benedetto, _Alto, _Moro, _Laureato, _Sportivo).
ragazzo(carlo, _Alto, _Moro, _Laureato, _Sportivo).
% donato e' basso
ragazzo(donato, false, _Moro, _Laureato, _Sportivo).
ragazzo(federico, _Alto, _Moro, _Laureato, _Sportivo).
ragazzo(giovanni, _Alto, _Moro, _Laureato, _Sportivo).
ragazzo(luca, _Alto, _Moro, _Laureato, _Sportivo).
ragazzo(gemello, _Alto, _Moro, _Laureato, _Sportivo).
% 1 ha 1 caratteristica (gemello di federico)
% quindi, federico non puo' essere ideale - al fratello gemello manca una
% caratteristica tra alto e moro
ragazzo(gemello) :- ragazzo(gemello, true, false, false, false);
                    ragazzo(gemello, false, true, false, false);
                    ragazzo(gemello, false, false, true, false);
                    ragazzo(gemello, false, false, false, true).

% 1 ha 3 caratteristiche - ?
% 3 hanno 2 caratteristiche

gemello :- benedetto; carlo; donato; giovanni; luca.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
alto(R) :- ragazzo(R, true, _Moro, _Laureato, _Sportivo).

% 4 alti
% federico deve essere uguale a gemello per le caratteristiche 'alto' e 'moro'
alto(federico) :- alto(gemello).
alto(carlo).
% uno tra benedetto, federico, giovanni, luca e' basso
alto(benedetto) :- \+ alto(federico) ; \+ alto(giovanni) ; \+ alto(luca).
alto(federico) :- \+ alto(benedetto) ; \+ alto(giovanni) ; \+ alto(luca).
alto(giovanni) :- \+ alto(benedetto) ; \+ alto(federico) ; \+ alto(luca).
alto(luca) :- \+ alto(benedetto) ; \+ alto(federico) ; \+ alto(giovanni).

%%%%%%%%%%%%%%%%%%
moro(R) :- ragazzo(R, _Alto, true, _Laureato, _Sportivo).
% federico deve essere uguale a gemello per le caratteristiche 'alto' e 'moro'
moro(federico) :- moro(gemello).

% scambiano 'moro', o Carlo o Luca sarebbe ideale
% quindi, o Carlo o Luca ha 3 caratteristiche
moro(luca) :- \+ moro(carlo).
moro(carlo) :- \+ moro(luca).

% due mori
mori(R1, R2) :- moro(R1), moro(R2).
mori(benedetto, carlo) :- \+ moro(donato), \+ moro(federico), \+ moro(giovanni), \+ moro(luca).
mori(benedetto, donato) :- \+ moro(carlo), \+ moro(federico), \+ moro(giovanni), \+ moro(luca).
mori(benedetto, federico) :- \+ moro(carlo), \+ moro(donato), \+ moro(giovanni), \+ moro(luca).
mori(benedetto, giovanni) :- \+ moro(carlo), \+ moro(donato), \+ moro(federico), \+ moro(luca).
mori(benedetto, luca) :- \+ moro(carlo), \+ moro(donato), \+ moro(federico), \+ moro(giovanni).
mori(carlo, donato) :- \+ moro(benedetto), \+ moro(federico), \+ moro(giovanni), \+ moro(luca).
mori(carlo, federico) :- \+ moro(benedetto), \+ moro(donato), \+ moro(giovanni), \+ moro(luca).
mori(carlo, giovanni) :- \+ moro(benedetto), \+ moro(donato), \+ moro(federico), \+ moro(luca).
mori(carlo, luca) :- \+ moro(benedetto), \+ moro(donato), \+ moro(federico), \+ moro(giovanni).
mori(donato, federico) :- \+ moro(benedetto), \+ moro(carlo), \+ moro(giovanni), \+ moro(luca).
mori(donato, giovanni) :- \+ moro(benedetto), \+ moro(carlo), \+ moro(federico), \+ moro(luca).
mori(donato, luca) :- \+ moro(benedetto), \+ moro(carlo), \+ moro(federico), \+ moro(giovanni).
mori(federico, giovanni) :- \+ moro(benedetto), \+ moro(carlo), \+ moro(donato), \+ moro(luca).
mori(federico, luca) :- \+ moro(benedetto), \+ moro(carlo), \+ moro(donato), \+ moro(giovanni).

%%%%%%%%%%%%%%%%%%%%%%
laureato(R) :- ragazzo(R, _Alto, _Moro, true, _Sportivo).
% quattro laureati

%%%%%%%%%%%%%%%%%%%%%
sportivo(R) :- ragazzo(R, _Alto, _Moro, _Laureato, true).
% quattro sportivi
% giovanni e federico hanno la stessa attitudine allo sport
sportivo(giovanni) :- sportivo(federico).
sportivo(federico) :- sportivo(giovanni).
