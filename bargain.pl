% https://www.ahapuzzles.com/logic/logic-puzzles/a-bargain/
% A Bargain Logic Puzzle

% Benny, Carla and Daniella each picked up a bargain in the January sales with all three items having a reasonable saving on the original selling price - the biggest being $11.50.
% From this information and the following clues, for each buyer, can you determine the item they bought, the color and the amount saved on the original selling price?

% 1. The blue jacket was not bought by Benny.
% 2. Daniella was pleased to have $9.00 knocked off the item she bought.
% 3. The red item had more knocked off than the umbrella.
% 4. The trousers did not have $5.00 knocked off and were not bought by Benny or Carla.

bought(benny, X) :- X = trousers; X = jacket.
bought(carla, jacket) :- bought(daniella, umbrella).
bought(daniella, jacket) :- bought(carla, umbrella).
% bought(carla, trousers)

saving(daniella, 9.00).
saving(X, 11.50) :- X = benny; X = carla.
saving(X, 5.00) :- X = benny; X = carla.
saving(benny, 5.00) :- saving(carla, 11.50).
saving(benny, 11.50) :- saving(carla, 5.00).
saving(carla, 5.00) :- saving(benny, 11.50).
saving(carla, 11.50) :- saving(benny, 5.00).

color(jacket, blue).
color(trousers, red).
color(umbrella, green).
