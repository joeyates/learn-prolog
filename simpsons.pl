parent(homer, bart).
parent(marge, bart).
parent(homer, lisa).
parent(marge, lisa).
parent(abraham, homer).
grandparent(X, Y) :- parent(X, Z), parent(Z, Y).
