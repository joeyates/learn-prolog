# From Learn Prolog Now! Exercise 2.4

word(astante, a,s,t,a,n,t,e).
word(astoria, a,s,t,o,r,i,a).
word(baratto, b,a,r,a,t,t,o).
word(cobalto, c,o,b,a,l,t,o).
word(pistola, p,i,s,t,o,l,a).
word(statale, s,t,a,t,a,l,e).

cross22(V1, H1) :- word(V1, _VA,   X, _VC, _VD, _VE, _VF, _VG), word(H1, _HA,   X, _HC, _HD, _HE, _HF, _HG), V1 \== H1.
cross24(V1, H2) :- word(V1, _VA, _VB, _VC,   X, _VE, _VF, _VG), word(H2, _HA,   X, _HC, _HD, _HE, _HF, _HG), V1 \== H2.
cross26(V1, H3) :- word(V1, _VA, _VB, _VC, _VD, _VE,   X, _VG), word(H3, _HA,   X, _HC, _HD, _HE, _HF, _HG), V1 \== H3.
cross42(V2, H1) :- word(V2, _VA,   X, _VC, _VD, _VE, _VF, _VG), word(H1, _HA, _HB, _HC,   X, _HE, _HF, _HG), V2 \== H1.
cross44(V2, H2) :- word(V2, _VA, _VB, _VC,   X, _VE, _VF, _VG), word(H2, _HA, _HB, _HC,   X, _HE, _HF, _HG), V2 \== H2.
cross46(V2, H3) :- word(V2, _VA, _VB, _VC, _VD, _VE,   X, _VG), word(H3, _HA, _HB, _HC,   X, _HE, _HF, _HG), V2 \== H3.
cross62(V3, H1) :- word(V3, _VA,   X, _VC, _VD, _VE, _VF, _VG), word(H1, _HA, _HB, _HC, _HD, _HE,   X, _HG), V3 \== H1.
cross64(V3, H2) :- word(V3, _VA, _VB, _VC,   X, _VE, _VF, _VG), word(H2, _HA, _HB, _HC, _HD, _HE,   X, _HG), V3 \== H2.
cross66(V3, H3) :- word(V3, _VA, _VB, _VC, _VD, _VE,   X, _VG), word(H3, _HA, _HB, _HC, _HD, _HE,   X, _HG), V3 \== H3.

crossword(V1, V2, V3, H1, H2, H3) :-
    cross22(V1, H1), cross24(V1, H2), cross26(V1, H3),
    cross42(V2, H1), cross44(V2, H2), cross46(V2, H3),
    cross62(V3, H1), cross64(V3, H2), cross66(V3, H3).
